<!DOCTYPE html>
<html>
    <head>
        <title>DAW2 DWS.</title>
        <meta charset="UTF-8">
    </head>
    <body>

        <h1>APLICACIÓN WEB</h1>
        <h2>DWS. GRUPO 7</h2>

        <hr/>

        <p>Inicio</p>

        <p>Elegir:</p>
        <ul>
            <li><a href="evaluable2php/index.php">Tema 2. Php</a> </li>
            <li><a href="evaluable3formularios/index.php">Tema 3. Formularios</a> </li>
            <li><a href="evaluable4ficheros/index.php">Tema 4. Ficheros</a> </li>
            <li><a href="evaluable5objetos/index.php">Tema 5. Objetos</a> </li>
            <li><a href="evaluable6mysql/index.php">Tema 6. Mysql</a> </li>
        </ul>

        <p>Datos del grupo:</p>

        <table border='1'>
            <tr>
                <th>Alumno#</th>
                <th>Nombre</th>

                <th>Actualizado</th>
                <th>Bitbucket</th>
                <th>Heroku</th>
            </tr>

            <tr>
                <td>Profesor</td>
                <td>Paco Aldarias</td>

                <td> 04/10/2018</td>
                <td>
                    <a href="https://bitbucket.org/pacoaldarias/pacoaldariasdws" target="tema2p">Bitbucket</a>
                </td>
                <td>
                    <a href ="https://pacoaldariasdws.herokuapp.com" target="heroku">Heroku</a>
                </td>
            </tr>

            <tr>
                <td>Alumno1</td>
                <td>Ignacio Vidal Vallejo</td>

                <td>14/10/2018</td>
                <td>
                    <a href="https://bitbucket.org/ignaciovidalvallejo/ignaciovidalvallejodwst2e1" target="tema2b1">Bitbucket</a>
                </td>
                <td>
                    <a href ="https://ignaciovidalvallejodwst2e1.herokuapp.com/" target="tema2h1">Heroku</a>
                </td>
            </tr>

            <tr>
                <td>Alumno2</td>
                <td>Frank Riesco</td>

                <td>15/10/2018 </td>
                <td>
                    <a href="https://bitbucket.org/frankriesco/frankriescodws" target="tema2b2">Bitbucket</a>
                </td>
                <td>
                    <a href ="https://frankriescodwsheroku.herokuapp.com/" target="tema2h2">Heroku</a>
                </td>
            </tr>

            <tr>
                <td>Alumno3</td>
                <td>Vicent Ribes Monz&oacute;</td>

                <td>24/11/2018</td>
                <td>
                    <a href="https://VicentRM@bitbucket.org/VicentRM/vicentribesdws" target="tema2b3">Bitbucket</a>
                </td>

                <td>
                    <a href ="https://vicentrmdws.herokuapp.com/" target="tema2h3">Heroku</a>
                </td>
            </tr>
        </table>

<ul>
    <li>
        <a href ="https://enlaceagoogledocs" target="docgru">Documentación del grupo</a>
    </li>
</ul>

    <hr/><pre> CEEDCV  DAW2 ED GRUPO7 </pre>

</body>
</html>

