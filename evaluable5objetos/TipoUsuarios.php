<?php
/**
 * Created by PhpStorm.
 * User: vicen
 * Date: 24/11/18
 * Time: 11:07
 */

class TipoUsuarios
{
    //Propiedades
    private $id;
    private $nombre;

    //Constructor
    public function __construct($id,$nombre){
        $this->id=$id;
        $this->nombre=$nombre;
    }

    //Metodos
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id=$id;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function setNombre($nombre) {
        $this->nombre=$nombre;
}
}