<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');
include_once("Enlaces.php");
include_once("TipoUsuarios.php");

class Ficheros {

  private $fe = "Enlaces.txt";
  private $fa = "Tipoenlaces.txt";
  private $ftu= "TiposUsuarios.txt";

  //**************************************
  public function getEnlaces() {
    $enlaces = array();

    $f = @fopen($this->fe, "r");

    if ($f) {
      $data = fgetcsv($f, 1000, ";");
      $cont = 0;
      while ($data) {
        $enlace = new Enlaces($data[0], $data[1], $data[2], $data[3]);
        $enlaces[$cont] = $enlace;
        $cont++;
        $data = fgetcsv($f, 1000, ";");
      }
      fclose($f);

      //echo "Num prof " . count($enlaces) . "<br>";
      //print_r($enlaces);
    } else {
      //echo "Error: No se puede abrir: " . $this->fe . "<br>";
    }

    return $enlaces;
  }

  //**************************************
  function getEnlace($enlace_) {
    $f = fopen($this->fe, "r");
    $data = fgetcsv($f, 1000, ";");
    $enlace = new Enlaces("", "");

    while ($data) {
      $idp = $data[0];
      //echo "Prof : " . $idp . " ==  " . $enlace_->getId() . "?<br>";
      if ($idp == $enlace_->getId()) {
        $enlace->setId($data[0]);
        $enlace->setNombre($data[1]);
        $enlace->setUrl($data[2]);
        $enlace->setTipoenlace($data[3]);
        break;
      }

      $data = fgetcsv($f, 1000, ";");
    }
    fclose($f);
    //echo "Leido Objeto: " . $enlace->getId() . " " . $enlace->getNombre() . "<br>";
    return $enlace;
  }

  //**************************************
  function grabarEnlace($enlace) {


    $f = fopen($this->fe, "a");
    $linea = $enlace->getId()
            . ";" . $enlace->getNombre()
            . ";" . $enlace->getUrl()
            . ";" . $enlace->getTipoenlace()
            . "\r\n";
    fwrite($f, $linea);

    fclose($f);
  }

  //**************************************
  function borrarEnlace($enlace) {

    // Leo todos los enlaces en un vector
    $enlaces = array();
    $enlaces = getEnlaces();

    // Borro el fichero
    ulink($this->fe);

    // Grabo todos los enlace del vector.
    if (count($enlaces) > 0) {
      foreach ($enlaces as $enlace) {
        grabarEnlace($enlace);
      }
    }
  }
//Funcion obtencion tipo de usuarios desde un fichero
    function getTiposUsuario() {
        $tipos=array();
        $ruta="TiposUsuarios.txt";
        $fichero=fopen($ruta,"r");
        if($fichero){
            $data=fgetcsv($fichero,1000,";");
            $cont=0;
            $i=0;
            while($data) {
                $tipo=new TipoUsuarios($data[0],$data[1]);
                $tipos[$cont]=$tipo;
                $cont++;
                $data = fgetcsv($fichero, 1000, ";");
            }
            fclose($fichero);
            return $tipos;
        }
    }
    //**************************************
    function getTipoUsuario($tipo_) {
        $f = fopen($this->ftu, "r");
        $data = fgetcsv($f, 1000, ";");
        $tipo = new TipoUsuarios("", "");

        while ($data) {
            $idp = $data[0];
            //echo "Prof : " . $idp . " ==  " . $enlace_->getId() . "?<br>";
            if ($idp == $tipo_->getId()) {
                $tipo->setId($data[0]);
                $tipo->setNombre($data[1]);
                break;
            }
            $data = fgetcsv($f, 1000, ";");
        }
        fclose($f);
        //echo "Leido Objeto: " . $enlace->getId() . " " . $enlace->getNombre() . "<br>";
        return $tipo;
    }
  //funcion grabar TipoUsuario
    //Funcion para grabar los tipos de usuarios en el fichero TiposUsuarios.txt
    function grabarTipoUsuario($tipo){
        $ruta="TiposUsuarios.txt";
        $fichero=fopen($ruta,"a");
        $linea=$tipo->getId()
            . ";" . $tipo->getNombre()
            . "\r\n";
        fwrite($fichero, $linea);

        fclose($fichero);
    }
    function actualizarTipoUsuario($tipo){
        $ruta="TiposUsuarios.txt";
        $fichero=fopen($ruta,"a");

        //inicializamos
        $i=1;
        $numlinea=$tipo->getId();
        $aux=array();
        $fichero=fopen("TiposUsuarios.txt","r");
        //Hacemos un bucle y vamos recogiendo linea a lina el archivo
        while ($linea=fgets($fichero)){
            if($i!=$numlinea){
                //Si la linea que deseamos modificar no es esta
                $aux[]=$linea;
            }else{ //si es la linea
                $linea=$tipo->getId().";".$tipo->getNombre();
                $aux[]=$linea;
            }
            $i++;
        }
        //cerramos el archivo
        fclose($fichero);
        //Convertimos el vector auxiiar en una cadena de texto (string) para guardarlo de nuevo
        $aux=implode($aux,"");
        //remplazamos el contenido del archivo con la cadena de texto (sin la linea eliminada)
        file_put_contents("TiposUsuarios.txt",$aux);
    }
    //Funcion para obtener el ultimo Id grabado en el fichero
    function getMaxId(){
        $tipos2=new Ficheros();
        $tipos=array();
        $tipos=$tipos2->getTiposUsuario();
        $id=1;
        for($i=0;$i<count($tipos);$i++){
            if($tipos[$i]->getId()>=$id) {
                $id=$tipos[$i]->getId()+1;
            }
        }
        return $id;
    }

}

?>