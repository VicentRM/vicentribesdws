<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');



$fa = "Tipoenlaces.txt";

//**************************************
function getEnlaces() {
    $enlaces = array();
    $enlace = array();

    $fe = "Enlaces.txt";
    $f = fopen($fe, "r");
    if ($f) {
        $data = fgetcsv($f, 1000, ";");
        $cont = 0;
        $i = 0;
        while ($data) {
            $enlace[0] = $data[0];
            $enlace[1] = $data[1];
            $enlace[2] = $data[2];
            $enlace[3] = $data[3];
            $enlaces[$cont] = $enlace;
            $cont++;
            $data = fgetcsv($f, 1000, ";");
        }
        fclose($f);

        return $enlaces;
    }
}

//**************************************
function grabarEnlace($enlace) {

    $fe = "Enlaces.txt";
    $f = fopen($fe, "a");
    $linea = $enlace[0]
        . ";" . $enlace[1]
        . ";" . $enlace[2]
        . ";" . $enlace[3]
        . "\r\n";
    fwrite($f, $linea);

    fclose($f);
}


//Funcion obtencion tipo de usuarios desde un fichero
function getTipoUsuario() {
    $tipos=array();
    $tipo=array();
    $ruta="TiposUsuarios.txt";
    $fichero=fopen($ruta,"r");
    if($fichero){
        $data=fgetcsv($fichero,1000,";");
        $cont=0;
        $i=0;
        while($data) {
            $tipo[0]=$data[0];
            $tipo[1]=$data[1];
            $tipos[$cont]=$tipo;
            $cont++;
            $data = fgetcsv($fichero, 1000, ";");
        }
        fclose($fichero);
        return $tipos;
    }
}

//Funcion para grabar los tipos de usuarios en el fichero TiposUsuarios.txt
function grabarTipoUsuario($tipo){
    $ruta="TiposUsuarios.txt";
    $fichero=fopen($ruta,"a");
    $linea=$tipo[0]
        . ";" . $tipo[1]
        . "\r\n";
    fwrite($fichero, $linea);

    fclose($fichero);
}
function actualizarTipoUsuario($tipo){
    $ruta="TiposUsuarios.txt";
    $fichero=fopen($ruta,"a");

    //inicializamos
    $i=1;
    $numlinea=$tipo[0];
    $aux=array();
    $fichero=fopen("TiposUsuarios.txt","r");
    //Hacemos un bucle y vamos recogiendo linea a lina el archivo
    while ($linea=fgets($fichero)){
        if($i!=$numlinea){
            //Si la linea que deseamos modificar no es esta
            $aux[]=$linea;
        }else{ //si es la linea
            $linea=$tipo[0].";".$tipo[1];
            $aux[]=$linea;
        }
            $i++;
    }
    //cerramos el archivo
    fclose($fichero);
    //Convertimos el vector auxiiar en una cadena de texto (string) para guardarlo de nuevo
    $aux=implode($aux,"");
    //remplazamos el contenido del archivo con la cadena de texto (sin la linea eliminada)
    file_put_contents("TiposUsuarios.txt",$aux);
    }


//Funcion para obtener el ultimo Id grabado en el fichero
function getMaxId(){
    $tipos=array();
    $tipos=getTipoUsuario();
    $tipo=array();
    $id=1;
    for($i=0;$i<count($tipos);$i++){
        $tipo=$tipos[$i];
        for($j=0;$j<count($tipo);$j++){
            if($tipo[0]>=$id){
                $id=$tipo[0]+1;
            }
        }
    }
    return $id;
}



?>