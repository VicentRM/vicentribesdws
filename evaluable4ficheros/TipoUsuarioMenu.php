<?php
include_once('Config.php');
include_once("funciones.php");
include_once("Ficheros.php");
?>
<!DOCTYPE html>
<html>
<head>
    <title> <?php echo titulo(); ?></title>
    <meta charset="UTF-8">
</head>
<body>

<?php cabecera(); ?>

<p>Gestión de tipos de Usuarios:</p>
<ul>
    <li><a href="TipoUsuarioFormulario.php">Alta </a> </li>
</ul>

<?php
echo "<p>Listado:</p>";

echo '<table border="1" with="100">';
echo '<tr>';
echo '<td>Id </td>';
echo '<td>Nombre </td>';
echo '<td>Borrar</td>';
echo '<td>Actualizar</td>';
echo '</tr>';



$tipos = getTipoUsuario();

if (count($tipos) > 0) {

    foreach ($tipos as $tipo) {
        echo "<tr>\n";
        echo "<td>" . $tipo[0] . "</td>\n";
        echo "<td>" . $tipo[1] . "</td>\n";
        echo '<td> <a href="TipoUsuarioBorrar.php?id=' . $tipo[0] . '">Borrar </td>';
        echo '<td> <a href="TipoUsuarioActualizar.php?id=' . $tipo[0] . '">Actualizar </td>';
        echo "</tr>\n";
    }

}

echo "</table>";
echo "<br/>";


volver();
pie();
?>

</html>
