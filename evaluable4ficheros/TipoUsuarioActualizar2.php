<?php
/**
 * Created by PhpStorm.
 * User: vicen
 * Date: 24/11/18
 * Time: 08:20
 */
include_once('Ficheros.php');
include_once('funciones.php');
error_reporting(E_ALL);
ini_set('display_errors','1');
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title> <?php echo titulo(); ?></title>
</head>
<body>
<?php
//Funcion para obtener los datos del formulario
function leerFormulario(){
    $id=recoge("id");
    $nombre=recoge("nombre");
    $tip=array();
    $tip[0]=$id;
    $tip[1]=$nombre;
    return $tip;
}

//Main

$tipo=leerFormulario();
if($tipo[0]!="" && $tipo[1]!="") {
    actualizarTipoUsuario($tipo);
    echo "Actualizando tipo de usuario.";
    echo '<a href="TipoUsuarioMenu.php">Seguir</a>';
} else {
    echo "Error: campos vacios";
    echo '<a href="TipoUsuarioFormulario.php">Volver a formulario</a>';
}

pie();
?>
</body>
</html>
